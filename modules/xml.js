var fs = require('fs');
var path = require('path');
var xml2js = require('xml2js');
var parser = new xml2js.Parser();

var aiNames = [
	"Private Sail",
	"Business Shipment",
	"Commercial Freighter",
	"Mining Carriage",
	"Mining Transport",
	"Mining Hauler",
	"Military Escort",
	"Military Minelayer",
	"Military Transporter"
];

var run = function(PATH, callback) {
	var allObjs = [];
	fs.exists(PATH, function(exists) {
		if (!exists) console.log('error: file does not exist: '+PATH);
		fs.readFile(PATH, function(err, file) {
			if (err) {
				console.log('fs err', err);
				return;
			}

			parser.parseString(file);

		});

		var parser = new xml2js.Parser();
		parser.addListener('end', function(result) {
			var objects = result.MyObjectBuilder_Sector.SectorObjects[0].MyObjectBuilder_EntityBase;
			objects.forEach(function(object) {
				var currentObject = {};
				if (object.CubeBlocks) {
					object.CubeBlocks.forEach(function(cubeBlock) {
						if (cubeBlock.MyObjectBuilder_CubeBlock) {
							cubeBlock.MyObjectBuilder_CubeBlock.forEach(function(cubeBlockElement) {
								if (cubeBlockElement.SubtypeName && cubeBlockElement.CustomName) {
									if (cubeBlockElement.SubtypeName[0].indexOf('BlockBeacon') !== -1) {
										currentObject.beaconName = cubeBlockElement.CustomName[0];
										if (aiNames.indexOf(cubeBlockElement.CustomName[0]) !== -1) {
											currentObject.ai = true;
										}
									}
									if (cubeBlockElement.SubtypeName[0].indexOf('BlockRadioAntenna') !== -1) {
										currentObject.antennaName = cubeBlockElement.CustomName[0];
									}
								}
							});
						}
					});
				}
				if (object.EntityId) {
					currentObject.id = object.EntityId[0];
				} else {
					console.log('entity has no id!');
				}
				if (object.IsStatic) {
					currentObject.isStatic = object.IsStatic[0];
				}
				if (object.DisplayName && object.DisplayName.length) {
					currentObject.name = object.DisplayName[0];
				}
				if (object.PositionAndOrientation) {
					currentObject.position = object.PositionAndOrientation[0].Position[0].$;
				}
				if (object.LinearVelocity) {
					currentObject.velocity = object.LinearVelocity[0].$;
				}
				if (object.$['xsi:type']) {
					currentObject.type = object.$['xsi:type'];

					if (object.$['xsi:type'] === 'MyObjectBuilder_FloatingObject' && object.Item) {
						currentObject.scrap = true;
						currentObject.scrapType = object.Item[0].PhysicalContent[0].SubtypeName[0];
					}
				}
				allObjs.push(currentObject);
			});

			writeJson();
		});
	});

	function writeJson() {
		fs.writeFile('public/data.json', JSON.stringify(allObjs), callback);
	}
};

module.exports = run;