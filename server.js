var PATH = process.argv[2];
if (!PATH) {
	console.log('Usage:\n');
	console.log('node server.js PATH_TO_SANDBOX_0_0_0_.sbs_FILE');
	console.log('for example: node server.js C:\\users\\se\\Application Data\\SpaceEngineersDedicated\\Saves\\Server Name\\SANDBOX_0_0_0_.sbs\n');
	console.log('you can also override webserver port as next argument');
	console.log('for example: node server.js C:\\path\\blahblah\\SANDBOX.sbs 3001');
	return;
}
var port = process.argv[3] || 3000;
var path = require('path');
var connect = require('connect');
var connectStatic = require('serve-static');

var app = connect();

var fs = require('fs');
var parseXML = require('./modules/xml.js');
var _ = require('underscore');

fs.exists(PATH, function(exists) {
	var currentlyParsing = false;
	if (exists) {
		console.log('SBS file exists, running web server at port ' + port);
		app.use(connectStatic('public'));

		app.listen(port);

		console.log('watching SANDBOX file for changes...');
		fs.watchFile(PATH, {
			persistent: true
		}, _.debounce(function() {
			if (currentlyParsing) {
				console.log('Already parsing right now, skipping');
				return;
			}
			currentlyParsing = true;
			console.log('SANDBOX file changed, parsing...');
			parseXML(PATH, function() {
				currentlyParsing = false;
				console.log('SANDBOX file parsed, created public/data.json succesfuly');
			});
		}), 10000);
		console.log('Parsing SANDBOX file for the first time...');
		currentlyParsing = true;
		parseXML(PATH, function() {
			currentlyParsing = false;
			console.log('SANDBOX file parsed, created public/data.json succesfuly');
		});
	} else {
		console.log('PATH is wrong, it points to non-existing file');
	}
});