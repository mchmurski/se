## Setting this up:
1. you need nodejs (http://nodejs.org/) (this will install nodejs, that command 'node' will be avaialble from your commandline)
2. clone this repo to your server
3. in the repo directory run 'npm install' - this will install all the dependencies
4. run 'node server.js' - this shows usage instructions
5. if you use IP to connect to your server, this tool will be available at http://IP:3000, for example http://123.234.456.123:3000, if you use domain, it will be like http://domain.com:3000
6. Tool will detect every server autosave and generate data basing on it.

## It's still early version - feedback and contributions are welcome!

note: this tool bases on autosave file named SANDBOX_0_0_0_.sbs, it detects game saving the autosave and generates new data for browser every time

## Example Usage:
node server.js C:\Users\user\Application Data\SpaceEngineersDedicated\Saves\MySave\SANDBOX_0_0_0_.sbs 3005
this will run the tool under port 3005 (http://domain.com:3005) with specified path to SANDBOX file