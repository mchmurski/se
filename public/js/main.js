if (!Detector.webgl) Detector.addGetWebGLMessage();

var mouse = new THREE.Vector2();

var colors = {
	MyObjectBuilder_VoxelMap: 0x333333,
	MyObjectBuilder_CubeGrid: 0xff0000,
	MyObjectBuilder_FloatingObject: 0x00ff00,
	MyObjectBuilder_Character: 0x0000ff
};
var sizes = {
	MyObjectBuilder_VoxelMap: 60,
	MyObjectBuilder_CubeGrid: 2,
	MyObjectBuilder_FloatingObject: 2,
	MyObjectBuilder_Character: 2
};
var geometries = {
	MyObjectBuilder_VoxelMap: new THREE.SphereGeometry(60, 32, 32),
	MyObjectBuilder_CubeGrid: new THREE.SphereGeometry(15, 32, 32),
	MyObjectBuilder_FloatingObject: new THREE.SphereGeometry(8, 32, 32),
	MyObjectBuilder_Character: new THREE.SphereGeometry(8, 32, 32)
};
var types = {
	MyObjectBuilder_VoxelMap: 'asteroids',
	MyObjectBuilder_CubeGrid: 'shipsAndStations',
	MyObjectBuilder_FloatingObject: 'floatingObjects',
	MyObjectBuilder_Character: 'players'
};

var objectTypes = {
	asteroids: [],
	shipsAndStations: [],
	floatingObjects: [],
	players: [],
	other: []
};
var objectIds = {};

var container, stats;

var camera, controls, scene, renderer;

var cross;

init();
animate();

function init() {

	camera = new THREE.PerspectiveCamera(30, window.innerWidth / window.innerHeight, 1, 100000000);
	camera.position.x = 30431260;
	camera.position.y = -55206111;
	camera.position.z = -73300898;

	// world

	scene = new THREE.Scene();

	// objects
	$.getJSON('data.json', function(data) {
		var extremes = {
			x: {
				min: 0,
				max: 0
			},
			y: {
				min: 0,
				max: 0
			},
			z: {
				min: 0,
				max: 0
			}
		};

		var asteroids = "";
		var players = "";
		var shipsAndStations = "";
		var floatingObjects = "";


		$.each(data, function(index, object) {
			var color;
			var geometry = geometries[object.type];

			if (object.type && colors[object.type] !== undefined) {
				color = colors[object.type];
			} else {
				// pink
				color = 0xff69b4;
			}
			var material = new THREE.MeshLambertMaterial({
				color: color,
				ambient: color * 8
			});
			object.position.x = parseFloat(object.position.x, 10);
			object.position.y = parseFloat(object.position.y, 10);
			object.position.z = parseFloat(object.position.z, 10);
			if (object.position.x < extremes.x.min) extremes.x.min = object.position.x;
			if (object.position.x > extremes.x.max) extremes.x.max = object.position.x;
			if (object.position.y < extremes.y.min) extremes.y.min = object.position.y;
			if (object.position.y > extremes.y.max) extremes.y.max = object.position.y;
			if (object.position.z < extremes.z.min) extremes.z.min = object.position.z;
			if (object.position.z > extremes.z.max) extremes.z.max = object.position.z;

			var mesh = new THREE.Mesh(geometry, material);
			mesh.position.x = object.position.x;
			mesh.position.y = object.position.y;
			mesh.position.z = object.position.z;
			mesh.updateMatrix();
			mesh.matrixAutoUpdate = false;

			mesh.name = object.id;

			scene.add(mesh);

			categorize(object);
		});

		setBoundaries(extremes, scene);
		var axes = buildAxes(extremes);
		scene.add(axes);

		var center = new THREE.Vector3(0, 0, 0);
		camera.lookAt(center);
		camera.position.x = extremes.x.min;
		camera.position.y = extremes.y.min;
		camera.position.z = extremes.z.min;
		renderHtml();
		render();
	});

	// lights
	var light = new THREE.AmbientLight(0x999999);
	scene.add(light);

	// renderer

	renderer = new THREE.WebGLRenderer({
		antialias: true
	});
	controls = new THREE.OrbitControls(camera, renderer.domElement);
	controls.addEventListener('change', render);

	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);

	renderer.setClearColor(0xffffff);

	container = document.getElementById('container');
	container.appendChild(renderer.domElement);

	renderer.domElement.addEventListener('mousedown', onDocumentMouseDown, false);

	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '0px';
	stats.domElement.style.zIndex = 100;
	container.appendChild(stats.domElement);

	window.addEventListener('resize', onWindowResize, false);
}

function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize(window.innerWidth, window.innerHeight);

	render();
}

function animate() {
	requestAnimationFrame(animate);
	controls.update();
}

function render() {
	renderer.render(scene, camera);
	stats.update();
}

function buildAxes(extremes) {
	var axes = new THREE.Object3D();
	axes.add(buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(extremes.x.max, 0, 0), 0xFF0000, false)); // +X
	axes.add(buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(extremes.x.min, 0, 0), 0xFF0000, false)); // -X
	axes.add(buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, extremes.y.max, 0), 0x00FF00, false)); // +Y
	axes.add(buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, extremes.y.min, 0), 0x00FF00, false)); // -Y
	axes.add(buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, extremes.z.max), 0x0000FF, false)); // +Z
	axes.add(buildAxis(new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, extremes.z.min), 0x0000FF, false)); // -Z

	return axes;
}

function buildAxis(src, dst, colorHex, dashed) {
	var geom = new THREE.Geometry(),
		mat;

	if (dashed) {
		mat = new THREE.LineDashedMaterial({
			linewidth: 3,
			color: colorHex,
			dashSize: 3,
			gapSize: 3
		});
	} else {
		mat = new THREE.LineBasicMaterial({
			linewidth: 3,
			color: colorHex
		});
	}

	geom.vertices.push(src.clone());
	geom.vertices.push(dst.clone());
	geom.computeLineDistances(); // This one is SUPER important, otherwise dashed lines will appear as simple plain lines

	var axis = new THREE.Line(geom, mat, THREE.LinePieces);

	return axis;
}

function setBoundaries(extremes, scene) {
	var width = extremes.x.max - extremes.x.min;
	var height = extremes.y.max - extremes.y.min;
	var depth = extremes.z.max - extremes.z.min;
	var offsetX = (extremes.x.max + extremes.x.min) / 2;
	var offsetY = (extremes.y.max + extremes.y.min) / 2;
	var offsetZ = (extremes.z.max + extremes.z.min) / 2;

	var mesh = new THREE.Mesh(new THREE.BoxGeometry(width, height, depth), new THREE.MeshNormalMaterial());
	var helper = new THREE.EdgesHelper(mesh, 0x000000);
	helper.position.set(offsetX, offsetY, offsetZ);
	helper.updateMatrix();

	scene.add(helper);
}

function categorize(object) {
	var type = types[object.type];
	if (!type) type = 'other';

	objectTypes[type].push(object);
	objectIds[object.id] = object;
}

function renderHtml() {
	$.each(objectTypes, function(index, objects) {
		var html = "";
		objects.forEach(function(object) {
			var beaconName = '';
			var antennaName = '';
			var name = '';

			if (object.name) {
				name = object.name;
			} else {
				name = object.id;
			}
			if (index === 'shipsAndStations') {
				if (object.antennaName) {
					antennaName = object.antennaName;
				} else if (object.beaconName) {
					beaconName = object.beaconName;
				}
			}

			html += '<li data-id="' + object.id + '" data-x="' + object.position.x + '" data-y="' + object.position.y + '" data-z="' + object.position.z + '"><a href="">';
			html += '<span class="name">' + name + '</span>';
			if (antennaName) {
				html += '<span class="antenna">a: ' + antennaName + '</span>';
			}
			if (beaconName) {
				html += '<span class="beacon">b: ' + beaconName + '</span>';
			}
			html += '</a></li>';
		});
		$('#category-' + index).html(html);
		$('#category-' + index).parent().find('.caret').before('<span class="badge">' + objectTypes[index].length + '</span>');
	});
}

$('.dropdown').on('click', 'a:not(.dropdown-toggle)', showItem);

function showItem(ev, id, goTo) {
	if (!ev && !id) return;
	var html = '';
	var object;
	if (ev) {
		goTo = true;
		ev.preventDefault();
		id = $(this).parents('li').attr('data-id');
	}
	object = objectIds[id];

	$.each(object, function(i, e) {
		var tmp = '';
		if (i === 'position') {
			tmp += '<div>x: ' + e.x + '</div>';
			tmp += '<div>y: ' + e.y + '</div>';
			tmp += '<div>z: ' + e.z + '</div>';

			tmp += '<button id="copy-button" title="Click to copy me." data-clipboard-text="GPS:' + object.name + ':' + e.x + ':' + e.y + ':' + e.z + ':" class="btn btn-default btn-position" href="#" role="button">Copy to clipboard</button>';
			e = tmp;
		}
		if (i === 'velocity') {
			tmp += '<div>x: ' + e.x + '</div>';
			tmp += '<div>y: ' + e.y + '</div>';
			tmp += '<div>z: ' + e.z + '</div>';
			e = tmp;
		}
		html += '<li class="list-group-item">' + i + ': ' + e + '</li>';
	});

	$('.panel-item .list-group').html(html);
	var client = new ZeroClipboard(document.getElementById("copy-button"));
	client.on("ready", function(readyEvent) {
		client.on("aftercopy", function(event) {
			$('#copy-button').text('Copied!');
		});
	});
	$('.panel-item').fadeIn();

	if (goTo) {
		camera.position.set(object.position.x, object.position.y, object.position.z - 500);
		controls.target.x = object.position.x;
		controls.target.y = object.position.y;
		controls.target.z = object.position.z;
	}
}
$('.panel .glyphicon-remove').click(function() {
	$('.panel').fadeOut();
});

function onDocumentMouseDown(event) {
	event.preventDefault();
	var currentclick = new Date().getTime();
	var goTo = false;
	if (window.lastclick) {
		if (currentclick - window.lastclick < 200) {
			goTo = true;
		}
	}
	window.lastclick = new Date().getTime();

	var vector = new THREE.Vector3();
	var raycaster = new THREE.Raycaster();
	var dir = new THREE.Vector3();
	if (camera instanceof THREE.OrthographicCamera) {

		vector.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1, -1); // z = - 1 important!

		vector.unproject(camera);

		dir.set(0, 0, -1).transformDirection(camera.matrixWorld);

		raycaster.set(vector, dir);

	} else if (camera instanceof THREE.PerspectiveCamera) {

		vector.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5); // z = 0.5 important!

		vector.unproject(camera);

		raycaster.set(camera.position, vector.sub(camera.position).normalize());

	}

	var intersects = raycaster.intersectObjects(scene.children, true);

	if (intersects.length > 0) {
		object = intersects[0].object;
		showItem(false, object.name, goTo);
	}
}
$('#help').click(function() {
	$('.panel-help').fadeIn();
});

$('#search').keyup(function() {
	var val = $(this).val();
	if (val === '') {
		$('.dropdown-menu').find('li').removeClass('hidden');
		$('.dropdown-menu').each(function() {
			$(this).parent().find('.badge').text($(this).find('li').length);
		});
	} else {
		var re = new RegExp(val, "i");
		$('.dropdown-menu').each(function() {
			$(this).find('li').each(function() {
				var $this = $(this);
				if (re.test($this.text())) {
					$this.removeClass('hidden');
				} else {
					$this.addClass('hidden');
				}
			});
			var visibleItemsLength = $(this).find('li:not(.hidden)').length;
			$(this).parent().find('.badge').text(visibleItemsLength);
		});
	}
});
$(document).on('keydown', function(e) {
	if (e.which === 70) {
		if (!$(this).is(':focus')) {
			e.preventDefault();
		}
		$('#search').focus();
	}
});